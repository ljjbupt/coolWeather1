package com.coolweather1.app.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.coolweather1.app.util.LogUtil;
import com.coolweather1.domian.City;
import com.coolweather1.domian.County;
import com.coolweather1.domian.Province;


public class CoolWeather1DB {
	/** 
	 * 数据库名称(lanjunjian)
	 **/
	public static final String DB_NAME="cool_weather";
	public static final String TAG="CoolWeather1DB";
	/**
	 * 数据库版本
	 **/
	public static final int VERSION= 1;
	private static CoolWeather1DB coolWeather1DB=null;
    private SQLiteDatabase db; 
	public synchronized static CoolWeather1DB getInstance(Context context){
		if(coolWeather1DB==null)
			coolWeather1DB=new CoolWeather1DB(context);
		return coolWeather1DB;
	}	
	/**
     * 将构造方法私有化
     **/
	private CoolWeather1DB(Context context){
		CoolWeatherOpenHelper dbHelper=new CoolWeatherOpenHelper(context, DB_NAME, null, VERSION);
		db =dbHelper.getWritableDatabase();
	}
	/**
	 * 关闭数据库
	 */
	public void closeDB(){
		db.close();
	}
	/**
	 * 插入省份信息
	 */
	public void insertProvince(Province province){
		if(db.isOpen()&&province!=null){
			String sql="insert into Province (province_name,province_code) values(?,?)";
			db.execSQL(sql, new Object[]{province.getProvinceName(),province.getProvinceCode()});
			LogUtil.i(TAG, "省份插入成功");
		}	
	}
	/**
	 * 从数据库中查询省份信息
	 **/
	public List<Province> queryProvinces(){
		if(db.isOpen()){
			String sql="select _id,province_name,province_code from Province";
			  Cursor cursor=db.rawQuery(sql,null);
			  if(cursor!=null&&cursor.getCount()>0){
				  List<Province> list=new ArrayList<Province>();
				  while(cursor.moveToNext()){
					  Province province=new Province();
					  province.setId( cursor.getInt(cursor.getColumnIndex("_id")));
					  province.setProvinceName(cursor.getString(cursor.getColumnIndex("province_name")));
					  province.setProvinceCode(cursor.getString(cursor.getColumnIndex("province_code")));
					  list.add(province);
				  }
				  cursor.close();
				  return list;
			  } 
		}
		 return null;
	}
	/**
	 * 插入城市信息
	 */
	public void insertCity(City city){
		if(db.isOpen()&&city!=null){
			String sql="insert into City (city_name,city_code,province_id) values(?,?,?)";
			db.execSQL(sql, new Object[]{city.getCityName(),city.getCityCode(),city.getProvinceId()});
			LogUtil.i(TAG, "城市插入成功");
		}	
	}
	/**
	 * 从数据库中查询某省下的城市信息
	 **/
	public List<City> queryCities(int provinceId){
		if(db.isOpen()){
			String sql="select _id,province_name,province_code,province_id from City where province_id =? ";
			  Cursor cursor=db.rawQuery(sql,(String[]) new String[]{String.valueOf(provinceId)});
			  if(cursor!=null&&cursor.getCount()>0){
				  List<City> list=new ArrayList<City>();
				  while(cursor.moveToNext()){
					  City city=new City();
					  city.setId( cursor.getInt(cursor.getColumnIndex("_id")));
					  city.setCityName(cursor.getString(cursor.getColumnIndex("city_name")));
					  city.setCityCode(cursor.getString(cursor.getColumnIndex("city_code")));
					  city.setProvinceId(provinceId);
					  list.add(city);
				  }
				  cursor.close();
				  return list;
			  } 
		}
		 return null;
	}
	/**
	 * 插入县镇信息
	 */
	public void insertCounty(County county){
		if(db.isOpen()&&county!=null){
			String sql="insert into County (county_name,county_code,city_id) values(?,?,?)";
			db.execSQL(sql, new Object[]{county.getCountyName(),county.getCountyCode(),county.getCityId()});
			LogUtil.i(TAG, "县镇插入成功");
		}	
	}
	/**
	 * 从数据库中查询某市下的县镇信息
	 **/
	public List<County> queryCounties(int cityId){
		if(db.isOpen()){
			String sql="select _id,county_name,county_code,city_id from County where city_id =? ";
			  Cursor cursor=db.rawQuery(sql,(String[]) new String[]{String.valueOf(cityId)});
			  if(cursor!=null&&cursor.getCount()>0){
				  List<County> list=new ArrayList<County>();
				  while(cursor.moveToNext()){
					  County county=new County();
					  county.setId( cursor.getInt(cursor.getColumnIndex("_id")));
					  county.setCountyName(cursor.getString(cursor.getColumnIndex("county_name")));
					  county.setCountyCode(cursor.getString(cursor.getColumnIndex("county_code")));
					  county.setCityId(cityId);
					  list.add(county);
				  }
				  cursor.close();
				  return list;
			  } 
		}
		 return null;
	}
	
}
